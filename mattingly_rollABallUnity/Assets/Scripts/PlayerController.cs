using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;
    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;

    //define variable for retractable wall that will disappear
    //when the first 12 pickups are collected, allowing the player
    //to access "level 2" of the playing field
    public GameObject retractableWall;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        count = 0;

        SetCountText();

        winTextObject.SetActive(false);
    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void SetCountText()
    {
        countText.text = "Count:" + count.ToString();

        if(count >= 12)
        {
            //when the first 12 pickups are collected, the retractable wall is deactivated
            //and the player can move to the "level 2" of the playing field
            retractableWall.SetActive(false);

        }

        //when the player collects all 20 pickups, "You WIN!" is displayed across the screen
        if(count >= 20)
        {
            winTextObject.SetActive(true);
        }
    }

    private void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);

            count = count + 1;

            SetCountText();
        }

        //if the player runs into a purple "bad" pickup,
        //the player's position is returned to a vector
        //at the beginning of "level 2"
        if (other.gameObject.CompareTag("BadPickUp"))
        {
            transform.position = new Vector3(1.7f, 0.5f, -9.2f);
        }
    }
}
